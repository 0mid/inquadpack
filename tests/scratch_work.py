import numpy as np
import autograd as ag
import autograd.numpy as anp
import scipy.integrate as spi
from inspect import getsource

import inquadpack as inq


def demo_calls():
    # Test centerpt(), halfwidths(), norm2(), volume(), halves(), etc.
    #
    # Store in C-contiguous mem blk
    hyprect = np.array([[0., 0., 0.],
                        [3., 4., 10]], order='C')
    x = np.arange(3.)/2.
    idx = 1
    lvlsets = [lambda x: anp.sqrt((x[0]-2)**2 + (x[1]+7)**2) - 9]
    sgns = [1., 1.]

    xc = inq.centerpt(hyprect)
    halfwidths_hyprect = inq.halfwidths(hyprect)
    l2_halfwidths_hyprect = inq.norm2(halfwidths_hyprect)
    print('''
hyprect =\n{}
    center = {}
    halfwidths = {}
    l2-norm of halfwidths = {}
    volume = {}
    Spliting hyprect into two halves along its largest extent, we get
    hyprect1 =\n{}
    hyprect2 =\n{}
    Sides except side idx={} =\n{}'''
          .format(hyprect,
                  xc,
                  halfwidths_hyprect,
                  l2_halfwidths_hyprect,
                  inq.volume(hyprect),
                  *inq.halves(hyprect),
                  idx,
                  inq.sides_except(hyprect, idx)))

    # Test autograd
    def integrand(x):
        return anp.sum(x**2+x)

    print('D(integrand) at center of hyprect =', ag.jacobian(integrand)(xc))
    print('where integrand is given by\n', getsource(integrand))

    # Test bound_fx_minus_fxc
    print('bound for |integrand(x)-integrand(xc)|'
          'in hyprect (lin. approx.) =',
          inq.bound_fx_minus_fxc(integrand, xc, l2_halfwidths_hyprect))

    # Test quad
    interval = np.array([0., 10.])
    I = spi.quad(integrand, *interval)
    print('''interval = {}
adaptive Gaussian quadrature = {}'''
          .format(interval, I[0]))

    # Test roots
    print('roots_fsolve of lvlsets =',
          inq.roots_fsolve(x, idx, lvlsets, interval))

    # Test roots_chebpoly()
    print('roots_chebpoly of lvlsets =',
          inq.roots_chebpoly(x, idx, lvlsets, interval))

    # Test eval_integrand
    print('integrand value (alg. 1) =',
          inq.eval_integrand(x, idx, integrand, lvlsets, sgns, interval))

    # Test eval_surf_integrand
    #
    # lvlsets[0] would be a function object (which is not iterable),
    # for `psi in lvlsets' in roots would result in a TypeError.
    # [lvlsets[0]] is a list (which is iterable).
    print('surface integrand value (alg. 2) =',
          inq.eval_surf_integrand(x, idx, integrand, [lvlsets[0]], interval))

    # Test nquad
    def func(*x):
        return 1 + 2*x[1] + 3*x[0]

    intlims = np.array([[0., 4.], [0., 4.]])
    print('''nquad of func = {}
    on intlims =\n{}
    where func is given by\n{}'''
          .format(spi.nquad(func, intlims),
                  intlims,
                  getsource(func)))

    # Test chebpts()
    def np_sin(x):
        return np.sin(x)

    xcheb = inq.chebpts(20, interval)
    y = np_sin(xcheb)
    pdeg = len(xcheb)-1
    p_coeffs = np.polyfit(xcheb, y, pdeg)
    p = np.poly1d(p_coeffs)
    p_rts = p.r
    p_rts_real = np.unique(p_rts[np.isreal(np.real_if_close(p_rts))])

    chebp_coeffs = np.polynomial.chebyshev.chebfit(xcheb, y, pdeg)
    chebp = np.polynomial.chebyshev.Chebyshev(chebp_coeffs)
    chebp_rts = chebp.roots()
    chebp_rts_real = np.unique(chebp_rts[
        np.isreal(np.real_if_close(chebp_rts))])
    print('Chebyshev points of 2nd-kind on interval {}:\n{}\n'
          .format(interval, xcheb))

    print('''Polynomial interpolant to \n{}
    coeffs in monomial basis =\n{}
        real roots=\n{}
    coeffs in Chebyshev basis =\n{}
        real roots =\n{}\n\n'''
          .format(
              getsource(np_sin),
              p_coeffs,
              p_rts_real,
              chebp_coeffs,
              chebp_rts_real))

    # Test inquad()
    n1, n2 = (3, 3)

    # Run inquad() ...
    def const_integrand(*x):
        return 1.

    def circle(x):
        return x[0]**2 + x[1]**2 - 1
    lvlsets = [circle]
    sgns = [-1.]
    is_surf = False
    A_ellipse = 0.

    # ... on one hyprect
    hyprect = np.array([[0, .99],
                        [.01, 1.01]])
    A_ellipse = inq.inquad(const_integrand, lvlsets, sgns, hyprect, is_surf)
    print('Area of ellipse \n{} on subhyprect \n{} \n= \n{}\n'
          .format(getsource(circle), hyprect, A_ellipse))

    # ... on whole region
    n1, n2 = (3, 3)
    x0pts = np.linspace(-1.1, 0, num=n1, endpoint=True)
    x1pts = np.linspace(0, 1.1, num=n2, endpoint=True)
    for i1 in range(n1-1):
        for i2 in range(n2-1):
            hyprect = np.array([[x0pts[i1], x1pts[i2]],
                                [x0pts[i1+1], x1pts[i2+1]]])
            print('\nhyprect\n', hyprect)
            A_ellipse += inq.inquad(const_integrand, lvlsets,
                                    sgns, hyprect, is_surf)
            print('cumulative area\n', A_ellipse)
