# test_inquad.py ---

# Copyright (C) 2017 Omid Khanmohamadi <>

# Author: Omid Khanmohamadi <>

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import numpy as np
import autograd.numpy as anp

import inquadpack as inq


class TestUnits:
    def test_centerpt(self):
        hyprects = [
            np.array([[0., 0., 0.],
                      [3., 4., 10.]])
        ]
        hyprects_centerpt = [
            np.array([1.5, 2., 5.])
        ]

        for hyprect, hyprect_centerpt in zip(hyprects, hyprects_centerpt):
            inq_hyprect_centerpt = inq.centerpt(hyprect)
            assert np.allclose(inq_hyprect_centerpt, hyprect_centerpt)

    def test_halfwidths(self):
        hyprects = [
            np.array([[-3., 0., 5.],
                      [-1., 4., 10.]])
        ]
        hyprects_halfwidths = [
            np.array([1., 2., 2.5])
        ]

        for hyprect, hyprect_halfwidth in zip(hyprects, hyprects_halfwidths):
            inq_hyprect_halfwidths = inq.halfwidths(hyprect)
            assert np.allclose(inq_hyprect_halfwidths, hyprect_halfwidth)

    def test_l2_halfwidths(self):
        hyprect_halfwidths = [
            np.array([0., 3., 4.])
        ]
        l2_hyprect_halfwidths = [
            5.
        ]

        for halfwidths, l2_halfwidths in zip(hyprect_halfwidths,
                                             l2_hyprect_halfwidths):
            inq_l2_halfwidths = inq.norm2(halfwidths)
            assert np.allclose(inq_l2_halfwidths, l2_halfwidths)

    def test_volume(self):
        hyprects = [
            np.array([[-3., 0., 5.],
                      [-1., 4., 10.]])
        ]
        hyprects_volume = [
            40.
        ]

        for hyprect, hyprect_volume in zip(hyprects, hyprects_volume):
            inq_hyprect_volume = inq.volume(hyprect)
            assert np.allclose(inq_hyprect_volume, hyprect_volume)

    def test_halves(self):
        hyprects = [
            np.array([[-2., 0., 4.],
                      [ 3., 4., 5.]])
            ,
            np.array([[-2., 0., 4.],
                      [ 3., 4., 10.]])
        ]

        # Each element of this list (separated from the next with a
        # lone comma on a new line) is a pair of halves.
        hyprects_halves = [
            (np.array([[-2., 0., 4.],
                       [0.5, 4., 5.]]),
             np.array([[0.5,  0., 4.],
                       [3.,   4., 5.]])
            )
            ,
            (np.array([[-2., 0., 4.],
                       [ 3., 4., 7.]]),
             np.array([[-2., 0., 7.],
                       [ 3., 4., 10.]])
            )
        ]

        for hyprect, hyprect_halves in zip(hyprects, hyprects_halves):
            inq_hyprect_halves = inq.halves(hyprect)
            assert np.allclose(inq_hyprect_halves, hyprect_halves)

    def test_sides_except(self):
        hyprects = [
            np.array([[-2., 0., 4.],
                      [ 3., 4., 5.]])
        ]

        # sides_except 0, 1, 2
        hyprects_sides_except = [
            (np.array([[0. , 4.],
                       [4., 5.]]),
             np.array([[-2., 4.],
                       [ 3., 5.]]),
             np.array([[-2., 0.],
                       [ 3., 4.]])
            )
        ]

        for hyprect, hyprect_sides_except in zip(hyprects,
                                                 hyprects_sides_except):
            ncols_hyprect = np.size(hyprect, axis=1)
            inq_hyprect_sides_except = tuple(
                    inq.sides_except(hyprect, idx)
                    for idx in range(ncols_hyprect)
                )
            assert np.allclose(inq_hyprect_sides_except,
                               hyprect_sides_except)

    def test_bound_fx_minus_fxc(self):
        fs_centerpts_l2_halfwidths = [
            (lambda x: anp.sum(x**2+x), # ... f: R^3 -> R
             np.array([0.5, 1., 3.]),   # centerpt in R^3 ...
             2.5                        # l2_halfwidths
            )
        ]

        bounds = [
            2.5*np.sqrt((2*0.5+1)**2 + (2*1.+1)**2 + (2*3.+1)**2)
        ]

        for f_centerpt_l2_halfwidths, bound in zip(
                fs_centerpts_l2_halfwidths, bounds):
            inq_bound = inq.bound_fx_minus_fxc(*f_centerpt_l2_halfwidths)
            assert np.allclose(inq_bound, bound)

    def test_roots_chebpoly_fsolve(self):
        xs_lvlsets_intervals = [
            (np.array([1., 2.]),                      # x (in R^{d-1})
             [lambda x: (x[0]-2.)*(x[1]+1.)*(x[2]-7.) # lvlsets (R^{d} -> R)
             ],
             np.array([-10, 10])                      # interval
            )
        ]
        roots = [
            (np.r_[2.], np.r_[-1.], np.r_[7.])
        ]

        for x_lvlsets_interval, rts in zip(xs_lvlsets_intervals, roots):
            x, lvlsets, interval = x_lvlsets_interval
            inq_rts_chebpoly = tuple(
                inq.roots_chebpoly(x, idx, lvlsets, interval)
                for idx in range(len(x)+1) # +1 ensures we insert after
                                           # last element of x too.
            )

            inq_rts_fsolve = tuple(
                inq.roots_fsolve(x, idx, lvlsets, interval)
                for idx in range(len(x)+1)
            )

            assert np.allclose(inq_rts_chebpoly, rts)
            assert np.allclose(inq_rts_fsolve, rts)

class TestIntegrations:
    def test_inquad_volume_2d(self):
        is_surf = False

        # integrands (beginning)
        #
        def const(*x):
            return 1.
        #
        # integrands (end)

        # lvlsets (beginning)
        #
        def circle(x):
            return x[0]**2 + x[1]**2 - 1.

        def line(x):
            return x[1]-x[0]

        def parabola(x):
            return x[1] - (2*(x[0]-1/2)**2 + 1/3)
        #
        # lvlsets (end)

        integrands_lvlsets_sgns_hyprects_npts = [
            # Unit disk enclosed in [-1.1, 1.1]x[-1.1, 1.1] hyprect.
            # Intersection is the whole unit disk.
            (const,             # integrand
             [circle],          # lvlsets
             [-1],              # sgns
             np.array([[-1.1, -1.1],  # hyprect
                       [ 1.1,  1.1]]),
             (10, 10)           # npts into which to break each dim of hyprect
            )
            ,

            # Unit disk enclosed in [0, 1/√2]x[0, 1.1] hyprect.
            # Intersection is the 45°-90° slice in 1st quadrant +
            # triangle under it.
            (const,
             [circle],
             [-1],
             np.array([[0,            0],
                       [1/np.sqrt(2), 1.1]]),
             (10, 10)
            )
            ,

            # Unit disk enclosed in [0, 1/√2]x[-1.1, 1.1] hyprect.
            # Intersection is the 45°-90° slice in 1st quadrant +
            # triangle under it + the -90°--(-45°) slice in 4th
            # quadrant + triangle over it.
            (const,
             [circle],
             [-1],
             np.array([[0,           -1.1],
                       [1/np.sqrt(2), 1.1]]),
             (10, 10)
            )
            ,

            # Unit disk enclosed in [-1.1, - 1/√2]x[-1/√2, 1/√2]
            # hyprect. Intersection is a -45°--45° lens in 2nd and 3rd
            # quadrants.
            (const,
             [circle],
             [-1],
             np.array([[-1.1,         -1/np.sqrt(2)],
                       [-1/np.sqrt(2), 1/np.sqrt(2)]]),
             (10, 10)
            )
            ,

            # Line y=x in [0, 1]x[0, 1] hyprect (unit square).
            # Intersection is the triangle under line.
            (const,
             [line],
             [-1],
             np.array([[0., 0.],
                       [1., 1.]]),
             (10, 10)
            )
            ,

            # Parabola y=2*(x-1/2)^2 + 1/3 in [0, 1]x[0, 1] hyprect
            # (unit square). Intersection is the area under parabola.
            (const,
             [parabola],
             [-1],
             np.array([[0., 0.],
                       [1., 1.]]),
             (10, 10)
            )
        ]

        areas = [
            # ~ 3.141592653589793
            np.pi
            ,

            # ~ 0.6426990816987241
            np.pi/8 + 1/4
            ,

            # ~ 1.2853981633974483
            2*(np.pi/8 + 1/4)
            ,

            # ~ 0.2853981633974483
            np.pi/4 - 1/2
            ,

            1/2
            ,

            1/2
        ]

        for f_psi_s_hyprect_ns, area in zip(
                integrands_lvlsets_sgns_hyprects_npts, areas):

            f, psi, s, hyprect, ns = f_psi_s_hyprect_ns
            n0, n1 = ns

            x0_start_end = hyprect[:, 0].T
            x1_start_end = hyprect[:, 1].T
            x0pts = np.linspace(*x0_start_end, num=n0, endpoint=True)
            x1pts = np.linspace(*x1_start_end, num=n1, endpoint=True)

            inq_area = 0.
            for i0 in range(n0-1):
                for i1 in range(n1-1):
                    hyprect_i0_i1 = np.array([[x0pts[i0],   x1pts[i1]],
                                              [x0pts[i0+1], x1pts[i1+1]]])
                    inq_area_new = inq.inquad(f, psi.copy(), s.copy(),
                                              hyprect_i0_i1, is_surf)
                    inq_area += inq_area_new

            print(inq_area, area)
            assert np.allclose(inq_area, area)
